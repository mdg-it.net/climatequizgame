/*
 * 
 * @author AIME JEAN
 * @param {type} param2
 * @param {type} param3
 */
/**
 * @return {string}
 */

function succes_modal(Message) {
    $('.info_modal').removeClass('alert-danger alert-dismissible')
        .addClass('alert-success alert-dismissible')
        .html(Message)
        .slideDown(1000)
        .fadeOut(10000)
        .show();
}

function show_errors_modal(Messageretourne) {
    $('.info_modal').removeClass('alert-success alert-dismissible')
        .html(Messageretourne)
        .slideDown(500)
        .fadeOut(10000)
        .addClass('alert-danger')
        .show();
}

/**
 * @param modals
 * @param BackGroundToAdd
 * @description remove old background color of header modal and add new backgroung color
 *
 * */
function headerModalCss(modals, BackGroundToAdd) {
    $(modals).find('.modal-header').addClass(BackGroundToAdd);
}

/**
 * @param modals
 * @param title
 * @description add modal title
 *
 * */
function titleModalCss(modals, title) {
    $(modals).find('.modal-title').html('<span class="fa fa-plus-circle"></span> ' + title);
}

function resetModalCustumCSs(modals) {
    console.log("Ato am reset zao ");
    $(modals).find('.modal-header').removeClass('bg-danger bg-info bg-secondary bg-warning bg-default bg-dark');
    $(modals).find('.btn-action')
        .removeClass('btn-outline-warning btn-outline-danger btn-outline-info ' +
            ' btn-outline-secondary btn-outline-default btn-outline-dark');
    $(modals).find('.modal-dialog').removeClass('modal-sm modal-lg');
    $(modals).find('.detail-form').html('');
}

/**
 * @param modals
 * @param buttonLabel
 * @param addClass
 * @description add css to button of modals after showing
 *
 * */
function buttonModalCss(modals, buttonLabel, addClass) {
    $(modals).find('.btn-action')
        .addClass(addClass)
        .html('<i class="fa fa-check"></i> ' + buttonLabel);
}

/**
 * @param modals
 * @param Mdclass
 * @description add type modal bootstrap with thoses values
 * small, lg, normal
 * */
function typeModal(modals, Mdclass) {
    $(modals).find('.modal-dialog').addClass(Mdclass);
}


/**
 * @param ChampId
 * @param divErreur
 * @description check an input form value
 *
 * */
function valider(ChampId, divErreur) {
    let champ = document.getElementById(ChampId);
    let valeur = champ.value;
    let errorDiv = $(divErreur);
    //console.log(errorDiv);
    let type = champ.getAttribute("data-type");
    let required = champ.getAttribute("data-required");
    let typeFormat = champ.getAttribute("data-format");
    //console.log(" Value " + valeur);
    let valide = false;

    //Tester une valeur numerique ou non
    if ((type === "numeric")) {
        if (isNaN(valeur) || valeur.length === 0 || valeur === null) {
            errorDiv.html(champ.getAttribute("data-msg-error")).css("color", "red");
            champ.style.borderColor = "#d9534f";
            errorDiv.show();
            valide = false;
        } else {
            champ.style.borderColor = "#ccc";
            errorDiv.hide();
            valide = true;
        }
        //Verification du type mail
    } else if (type === "email") {
        let reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
        //si le champs est obligatoire, onverifie directement
        if (required === '1') {
            if (reg.test(valeur)) {
                champ.style.borderColor = "#ccc";
                errorDiv.hide();
                valide = true;
            } else {
                champ.style.borderColor = "#d9534f";
                valide = false;
                errorDiv.html(champ.getAttribute("data-msg-error")).css("color", "red");
                errorDiv.show();
            }
        } else { // sinon on laisse libre mais si le contenu n'est pas vide, on verifie l'adresse
            //console.log("ato fa misy ", valeur.length)
            if (valeur.length > 0) {
                if (reg.test(valeur)) {
                    champ.style.borderColor = "#ccc";
                    errorDiv.hide();
                    valide = true;
                } else {
                    champ.style.borderColor = "#d9534f";
                    valide = false;
                    errorDiv.html(champ.getAttribute("data-msg-error")).css("color", "red");
                    errorDiv.show();
                }
            } else {
                champ.style.borderColor = "#ccc";
                errorDiv.hide();
                valide = true;
            }
        }
        //Type select
    } else if (type === "select") {
        if (valeur === null || valeur === 'Selectionner') {
            errorDiv.html(champ.getAttribute("data-msg-error")).css("color", "red");
            errorDiv.show();
            console.log('Ato le erreur select');
            valide = false;
        } else {
            errorDiv.hide();
            valide = true;
        }
    } else if (valeur === "" || valeur === null) {
        errorDiv.html(champ.getAttribute("data-msg-error")).css("color", "red");
        champ.style.borderColor = "#d9534f";
        errorDiv.show();
        valide = false;
    } else {
        switch (typeFormat) {
            case 'phone':
                if (checkPhoneNumber(valeur.trim())) {
                    champ.style.borderColor = "#ccc";
                    errorDiv.hide();
                    valide = true;
                } else {
                    errorDiv.html(champ.getAttribute("data-format-error")).css("color", "red");
                    champ.style.borderColor = "#d9534f";
                    errorDiv.show();
                    valide = false;
                }
                break;
            case 'cin':
                if (checkCINorCnaps(valeur.trim())) {
                    champ.style.borderColor = "#ccc";
                    errorDiv.hide();
                    valide = true;
                } else {
                    errorDiv.html(champ.getAttribute("data-format-error")).css("color", "red");
                    champ.style.borderColor = "#d9534f";
                    errorDiv.show();
                    valide = false;
                }
                break;
            case 'stat':
                if (checkStat(valeur.trim())) {
                    champ.style.borderColor = "#ccc";
                    errorDiv.hide();
                    valide = true;
                } else {
                    errorDiv.html(champ.getAttribute("data-format-error")).css("color", "red");
                    champ.style.borderColor = "#d9534f";
                    errorDiv.show();
                    valide = false;
                }
                break;
            case 'nif':
                if (checkNif(valeur.trim())) {
                    champ.style.borderColor = "#ccc";
                    errorDiv.hide();
                    valide = true;
                } else {
                    errorDiv.html(champ.getAttribute("data-format-error")).css("color", "red");
                    champ.style.borderColor = "#d9534f";
                    errorDiv.show();
                    valide = false;
                }
                break;
            default:
                champ.style.borderColor = "#ccc";
                errorDiv.hide();
                valide = true;
                break;
        }
    }
    return valide;
}

/*
* Effectuer une validation d'un formulaire selectionne
* */
function validerForm(formulaire) {
    var erreur = 0;
    var formValide = false;
    formulaire.find('.field').each(function (i, v) {
        var champsID = $(v).attr("id");
        var classDiv = '.error-' + champsID;
        //console.log(champsID+" "+classDiv);
        if (!valider(champsID, classDiv)) {
            erreur++;
        }
    });
    if (erreur === 0) {
        formValide = true;
    }
    console.log("Erreur " + erreur + " resultat " + formValide)
    return formValide;
}

/*
 * Fonction pour effectuer un cocher tous et decocher tous
 * */
$(document).on('click', '.cocheTout', function () {
    var cases = $(".cases").find(':checkbox'); // on cherche les checkbox dans le bloc cases
    if (this.checked) { // si 'cocheTout' est coché
        cases.prop('checked', true); // on coche les cases
        $(this).prop("title", "Decocher tous");
    } else { // si on décoche 'cocheTout'
        cases.prop('checked', false);// on coche les cases
        $(this).prop("title", "Cocher tous");
    }

});

//Fonction pour envoyer des donnnees
function ajaxSaveorUpdate(postlink, datas) {
    return $.ajax({
        url: postlink,
        type: "POST",
        data: datas
    })
}

function ajaxSaveorUpdateData(form) {
    console.log(form.serialize());
    if (validerForm(form)) {
        return $.ajax({
            url: form.attr('action'),
            type: "post",
            data: form.serialize()
        })
    }
}

function executeDElete(params) {
    ajaxGetHtmlContents(params.lnk, params)
        .done(function (data) {
            console.log('data ', data);
            if (data > 0) {
                location.reload();
            } else {
                show_errors_modal('Unable to delete this record')
            }
        }).fail(function (err) {
        show_errors_modal(err.responseJSON.message)
        //console.log('err', err)
    })
}

function ajaxSaveorUpdateMultipartForm(postlink, datas) {
    return $.ajax({
        url: postlink,
        enctype: 'multipart/form-data',
        type: "post",
        processData: false,
        contentType: false,
        cache: false,
        data: datas
    })
}

//
function reloaddata(path) {
    return $.ajax({
        url: path,
        type: "get"
    })
}

function reloadDataFromDB(reloadlLink) {
    reloaddata(reloadlLink).done(function (data) {
        $('.table-detail').html(data);
        console.log(data);
    }).fail(function (jqXHR) {
        show_errors_modal(jqXHR.responseJSON.message);
        console.log(jqXHR)
    });
}

/**
 * recuperer un pqrtie de donnee de la BDD
 * */

function ajaxGetHtmlContents(paths, datas) {
    return $.ajax({
        url: paths,
        type: "get",
        data: datas
    });
}

/** recuperer link and ID*/
function GetIdAndLink_to_send(id, lnk) {
    this.id = id;
    this.lnk = lnk;
}


function activateButton(selecteur) {
    $(selecteur).prop("disabled", true);
}

function disableButton(selecteur) {
    $(selecteur).prop("disabled", false);
}


function datatableMinimalInfo() {
    jQuery(document).find('.table').DataTable({
        "paging": false,
        "searching": false,
        "info": false,
        "language": {
            "zeroRecords": "Aucune donnée trouvée",
            "infoEmpty": "Aucune donnée trouvée"
        }
    });
}

function clearDatatable() {
    jQuery(document).find('.table').DataTable().clear().draw();
}

function datatableAllInfo() {
    jQuery(document).find('.table').DataTable({
        "searching": false,
        "language": {
            "zeroRecords": "Aucune donnée trouvée",
            "lengthMenu": "Affichage _MENU_ enreg. par page",
            "infoEmpty": "Aucune donnée trouvée",
            "info": "Affichage page _PAGE_ sur _PAGES_"
        }
    });
}

function datatableMinmalWithSearch() {
    jQuery(document).find('.table').DataTable({
        "paging": false,
        "searching": true,
        "info": false,
        "language": {
            "zeroRecords": "Aucune donnée trouvée",
            "infoEmpty": "Aucune donnée trouvée"
        }
    });
}

/**
 * Activate Bootstrap datePicker
 * */
function datePickerTrigger() {

    let config = {
        autoclose: true,
        language: 'fr',
        todayHighlight: true,
        clearBtn: true,
        endDate: new Date()
    };
    jQuery(document).find('.bs-datepicker').datepicker(config);


}

function setDateToday() {
    //Set default date today
    let vDate = new Date();
    let vDay = new Date(vDate.getFullYear(), vDate.getMonth(), vDate.getDate());
    jQuery(document).find('.bs-datepicker').datepicker('update', vDay);
}

/*
 Format date 02022002 return 02/02/2002
* */
function formatNumericDate(selecteur) {
    //console.log("fdafhaHFHAfh;aFHAfaf", selecteur.val())

    let numericDate = selecteur.val();

    if (numericDate.length === 8) {
        let day = numericDate.substring(0, 2);
        let month = numericDate.substring(2, 4);
        let year = numericDate.substring(4);
        //console.log(day + '/' + month + '/' + year);

        let vDayUpdate = new Date(year, (month - 1), day);
        //console.log('date', vDayUpdate);
        jQuery(document).find('.bs-datepicker').datepicker('update', vDayUpdate);
    }
}

/**
 * @param tbodyselector
 * @description total of columns of the first row in the selected table
 * @return number
 * */
function getTotalColonneTable(tbodyselector) {
    return document.querySelector(tbodyselector)
        .rows[0].cells.length;
}

function getTableRow(tbodyselector, rowIndex) {
    return document.querySelector(tbodyselector)
        .rows[rowIndex];
}

function getOneCellTable(tbodyselector, rowIndex, colNumber) {
    return document.querySelector(tbodyselector)
        .rows[rowIndex].cells[colNumber];
}

/**
 * @param tbodyselector
 * @param excludes
 * @param data
 * @description Update values of the
 * selected row and excludes update with excludes index values
 * */
function insertAllDataToCellTable(tbodyselector, data, excludes) {
    let totalColumn = getTotalColonneTable(tbodyselector);
    for (let i = 0; i < totalColumn; i++) {
        if (!excludes.includes(i)) {
            document.querySelector(tbodyselector)
                .rows[0].cells[i].innerHTML = data[i];
        }
    }
}

/**
 * @param tables
 * @param row
 * @description delete the selected row
 * @return
 * */
function deleteRowOnClick(row, tables) {
    let ligne = document.querySelector(row);
    let i = ligne.parentNode.parentNode.rowIndex;
    // console.log('indesx ', i);
    document.getElementById(tables).deleteRow(i);
}

/**
 * @param tbodyselector
 * @description copy the first row and append at the end
 * @return
 * */
function cloneRow(tbodyselector) {
    let cl1 = document.querySelector(tbodyselector).firstElementChild;
    let cp = cl1.cloneNode(true);
    document.querySelector(tbodyselector).appendChild(cp);
}

/**
 * Check Madagascar phone number format +261 32 55 325 01 / 032 11 454 88
 * @param valueToCheck String
 * @returns boolean
 * **/
function checkPhoneNumber(valueToCheck) {
    let check = false;
    console.log(valueToCheck.length);
    if (valueToCheck.length === 17 || valueToCheck.length === 13) {
        console.log("Length ok ", valueToCheck.trim().charAt(0));
        if (valueToCheck.trim().charAt(0) === '+') {
            console.log("debut +");
            if ((valueToCheck.charAt(4) === " ") && (valueToCheck.charAt(7) === " ") && (valueToCheck.charAt(10) === " ") && (valueToCheck.charAt(14) === " ")) {
                check = true;
                console.log("valide le 16");
            }
        } else if (valueToCheck.trim().charAt(0) === '0') {
            if ((valueToCheck.charAt(3) === " ") && (valueToCheck.charAt(6) === " ") && (valueToCheck.charAt(10) === " ")) {
                check = true;
                console.log("valide le 13");
            }
        }
    }
    return check;
}

/**
 * Check CIN or Cnaps MADAGASCAR format 409 000 125 000
 * @param valueToCheck String
 * @returns boolean
 * **/
function checkCINorCnaps(valueToCheck) {
    var check = false;
    console.log(valueToCheck.length);
    if (valueToCheck.length === 15) {
        console.log("Length ok ", valueToCheck.trim().charAt(0));
        if ((valueToCheck.charAt(3) === " ") && (valueToCheck.charAt(7) === " ") && (valueToCheck.charAt(11) === " ")) {
            check = true;
            console.log("valide Cin ou cnaps");
        }
    }
    return check;
}

/**
 * Check NIF MADAGASCAR format  4555 96201
 * @param valueToCheck String
 * @returns boolean
 * **/
function checkNif(valueToCheck) {
    let check = false;
    console.log(valueToCheck.length);
    if (valueToCheck.length === 10) {
        console.log("Length ok ", valueToCheck.trim().charAt(0));
        if ((valueToCheck.charAt(4) === " ")) {
            check = true;
            console.log("valide NIF");
        }
    }
    return check;
}

/**
 * Check stat format 15110 10 1908 0 02929
 * @param valueToCheck String
 * @returns boolean
 * **/
function checkStat(valueToCheck) {
    let check = false;
    console.log(valueToCheck.length);
    if (valueToCheck.length === 21) {
        console.log("Length ok ", valueToCheck.trim().charAt(0));
        if ((valueToCheck.charAt(5) === " ") && (valueToCheck.charAt(8) === " ")
            && (valueToCheck.charAt(13) === " ") && (valueToCheck.charAt(15) === " ")) {
            check = true;
            console.log("valide stat");
        }
    }
    return check;
}

/*
 * Initialiser un champs de type select avec le plugins select2
 * https://select2.org/getting-started/installation
 * */
function select2Init() {
    jQuery(document).find('.select-single, .select-multiple').select2({
        placeholder: 'Please select value',
        theme: "bootstrap",
        containerCssClass: ':all:',
        width: null,
    });
    $(".select-multiple").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });
    console.log("initialisation du select2")
}

/*
* Selectionner des items par defauts
* */

function selectedItemsSelect2(params, links, selecteur) {
    $.when(ajaxGetHtmlContents(links, params))
        .then(function (donnee) {
            //console.log(donnee);
            $(document).find(selecteur).val(donnee);
        }).always(function () {
        console.log('trigger ');
        $(document).find(selecteur).trigger('change');
    })
}

/*
* Initialiser le plugins viewBox pour afficher un image dans un modal
* https://www.jqueryscript.net/demo/Tiny-Responsive-Lightbox-Gallery-Plugin-For-jQuery-Viewbox/
* */
function viewBoxInit() {
    $(document).find($('.image-link')).viewbox(
        {
            setTitle: false,
            margin: 60,
            resizeDuration: 300,
            openDuration: 200,
            closeDuration: 200,
            closeButton: false,
            navButtons: false,
            closeOnSideClick: true,
            nextOnContentClick: false
        }
    );
    //console.log('view box initialsed');
}

/*
* Initialiser un chaps de type file avec un plugins bootstrap file input
* http://plugins.krajee.com/file-input
* */
function bootstrapFileInput() {

    jQuery(document).find(".bs-file-input").fileinput(
        {
            showUpload: false,
            previewFileType: 'any',
            theme: "fa",
            maxFileSize: 50000,
            overwriteInitial: false,
            maxFileCount: 100,
            allowedFileExtensions: ["jpeg", "png", "jpg","pdf"]
        }
    );

}

function bootstrapAvatar() {
    jQuery(document).find(".avatar-1").fileinput({
        maxFileSize: 50000,
        showClose: false,
        theme: "fa",
        showUpload: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="fa fa-trash-o"></i>',
        removeTitle: 'remove this file',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="show-avatar?filename=user.png" ' +
            ' alt="Your Avatar" style="width: 40px; height: 40px" ><h6   ' +
            ' class="text-muted">Clic to browse </h6>',
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });
}

/*
*
* Export a html page to pdf using html2pdf library
* link: https://github.com/eKoopmans/html2pdf.js
*
* */

function exportPdf(filename, elementToExport) {
    let element = document.getElementById(elementToExport);
    let opt = {
        margin: 0.5,
        filename: filename + '.pdf',
        image: {type: 'jpeg', quality: 0.98},
        html2canvas: {scale: 2},
        jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
    };

    html2pdf().set({
        pagebreak: {mode: 'avoid-all', before: '.page-break1'}
    });

    // Old monolithic-style usage:
    html2pdf(element, opt);
}



