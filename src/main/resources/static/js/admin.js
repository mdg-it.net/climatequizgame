
$(document).ready(function () {

    // connect to game
    $('#admin-connect-game').on('click',function (e){
        connect();
        e.preventDefault();
        $('#admin-connect-game').addClass('hidden');
        $('#admin-start-game').removeClass('hidden');
        activateButton('#admin-start-game');
    });

    // hide first-page and go in second-page
    $('#admin-start-game').on('click',function (event){
        event.preventDefault();
        $('#admin-first').addClass('hidden');
        $('#admin-second').removeClass('hidden');
        //create game
        ajaxGetHtmlContents("/create-game", {})
            .done(function (donnee) {
                console.log(donnee);
            }).fail(function (err) {
            console.log("erreur de chargement");
        });
        // send message to start game
        messageInput.value = "Start game";
        sendMessage();
        // set time out to finished game
        setTimeout(endGame, 300000);
    });

    // end game after 05 min
    function endGame(){
        // change content of admin-third
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                   if(item.user.loginName === "user1"){
                       team1Name = '@'+item.teamName;
                       team1Score = item.score;
                   }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });

                if(team1Score === team2Score){
                    $('#admin-secondRound-game').removeClass('hidden');
                    // score
                    $('#team-gagnant').html(team1Name);
                    $('#team-gagnant-score').html(team1Score);
                    $('#team-perdant').html(team2Name);
                    $('#team-perdant-score').html(team2Score);
                    // image
                    $("#scoregagnatperdant").addClass('hidden');
                    $("#scoreegalite").removeClass('hidden');
                }else{
                    $('#admin-new-game').removeClass('hidden');
                    // score
                    if(team1Score > team2Score){
                        $('#team-gagnant').html(team1Name);
                        $('#team-gagnant-score').html(team1Score);
                        $('#team-perdant').html(team2Name);
                        $('#team-perdant-score').html(team2Score);
                    }else{
                        $('#team-perdant').html(team1Name);
                        $('#team-perdant-score').html(team1Score);
                        $('#team-gagnant').html(team2Name);
                        $('#team-gagnant-score').html(team2Score);
                    }
                    // image
                    $("#scoreegalite").addClass('hidden');
                    $("#scoregagnatperdant").removeClass('hidden');
                }
                // send message
                messageInput.value = "End game1";
                sendMessage();

            }).fail(function (err) {
                console.log("erreur de chargement");
            });
    }


    function endGame2(){
        // change content of admin-third
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                    if(item.user.loginName === "user1"){
                        team1Name = '@'+item.teamName;
                        team1Score = item.score;
                    }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });

                if(team1Score === team2Score){
                    $('#admin-secondRound-game').addClass('hidden');
                    $('#admin-new-game').removeClass('hidden');
                    // score
                    $('#team-gagnant').html(team1Name);
                    $('#team-gagnant-score').html(team1Score);
                    $('#team-perdant').html(team2Name);
                    $('#team-perdant-score').html(team2Score);
                    // image
                    $("#scoregagnatperdant").addClass('hidden');
                    $("#scoreegalite").removeClass('hidden');
                }else{
                    $('#admin-secondRound-game').addClass('hidden');
                    $('#admin-new-game').removeClass('hidden');
                    // score
                    if(team1Score > team2Score){
                        $('#team-gagnant').html(team1Name);
                        $('#team-gagnant-score').html(team1Score);
                        $('#team-perdant').html(team2Name);
                        $('#team-perdant-score').html(team2Score);
                    }else{
                        $('#team-perdant').html(team1Name);
                        $('#team-perdant-score').html(team1Score);
                        $('#team-gagnant').html(team2Name);
                        $('#team-gagnant-score').html(team2Score);
                    }
                    // image
                    $("#scoreegalite").addClass('hidden');
                    $("#scoregagnatperdant").removeClass('hidden');
                }
                // send message
                messageInput.value = "End game2";
                sendMessage();

            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    }

    // new game
    $('#admin-new-game').on('click',function (event){
        event.preventDefault();
        // update data in database
        ajaxGetHtmlContents("/update-data", {})
            .done(function (donnee) {
                console.log(donnee);
            }).fail(function (err) {
                console.log("erreur de chargement");
            });
        // send message
        messageInput.value = "finished game";
        sendMessage();
    });

    // restart game for user1
    $('#restart-user1').on('click',function (event){
        event.preventDefault();
        // update data in database
        ajaxGetHtmlContents("/update-data/user1", {})
            .done(function (donnee) {
                console.log(donnee);
            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });

    // restart game for user2
    $('#restart-user2').on('click',function (event){
        event.preventDefault();
        // update data in database
        ajaxGetHtmlContents("/update-data/user2", {})
            .done(function (donnee) {
                console.log(donnee);
            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });

    // second round
    $('#admin-secondRound-game').on('click',function (event){
        event.preventDefault();
        // send message
        messageInput.value = "Second Round";
        sendMessage();
        // set time out to finished second round
        setTimeout(endGame2, 60000);
    });

});