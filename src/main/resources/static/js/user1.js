$(document).ready(function () {

    // hide first-page and go in second-page
    $('#team-formulaire').on('submit',function (event){
        event.preventDefault();
        // send request to create a team
        if (validerForm($(this))) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (data) {
                    console.log(data);
                    if(data !== null && data.teamName !== null){
                        $('#user1-first').addClass('hidden');
                        $('#user1-second').removeClass('hidden');
                        $('.team-name').html(data.teamName)
                        // connect in web secket
                        connect();
                    }else{
                        $('#message-info').html("You couldn't connect because the session is used.");
                    }
                },
                error: function (e) {
                    console.log("erreur de creation team");
                }
            });
        }
    });

    // send data after choise response
    var inputradio = document.querySelectorAll('.start-page-r input[type="radio"]');
    inputradio.forEach(function (input){
        input.addEventListener('change',function (e){
            e.preventDefault();
            var form = $(this).parent().parent().parent().parent();
            if (validerForm(form)) {
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (data) {
                        console.log(data);
                        if (data !== "") {
                            // make the indicator active
                            $('.quest-indic-'+data.split('-')[0]).addClass('active');
                            // team score
                            let scorevalue = $('.team-score').html();
                            let resultat = parseInt(scorevalue) + parseInt(data.split('-')[1]);
                            $('.team-score').html(resultat);
                            // play song for condition wrong or right
                            if(parseInt(data.split('-')[1]) === 1){
                                // change the image in the interface
                                let indexold = parseInt(scorevalue) + 1;
                                let indexnew = parseInt(resultat)+1;
                                let indexoldname = 'question'+indexold;
                                let indexnewname = 'question'+indexnew;
                                $('.'+indexoldname+'').addClass("hidden");
                                $('.'+indexnewname+'').removeClass("hidden");

                                document.getElementById("responseright").play();
                            }else{
                                document.getElementById("responsewrong").play();
                            }

                            // send message to start game
                            messageInput.value = "score-"+data;
                            sendMessage();

                        } else {
                            console.log("question vide");
                        }
                    },
                    error: function (e) {
                        console.log("erreur");
                        // Handle the error as needed
                    }
                });
            }
            // disabled all input
            form.find('input').prop("disabled",true);
            form.find('.correct').css("animation","pulse 2s infinite")
            if(!$(this).parent().hasClass('correct')){
                $(this).parent().css("animation","pulsefalse 2s infinite");
            }

        })
    })

    // save participant
    $('#form-participant').on('submit',function (e){
        e.preventDefault();
        if (validerForm($(this))) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (data) {
                    console.log(data);
                    // append player in list
                    if(data != null && data.nom != null){
                        let numberplayer = (($('.user1-list-player li') === null)? 0 : parseInt($('.user1-list-player li').length) + 1);
                        $('.user1-list-player').append("<li class=''><p class='text-left'><span class='nu-part'>0"+numberplayer+"</span><span class='name-team'>"+data.nom+"</span></p></li>");
                        // Message for user
                        $('#message1').removeClass('hidden');
                        $('#message1').html("Participant save with success");
                        $('#message1').css("color",'#2196F3');
                        setTimeout(function() {
                            $('#message1').addClass('hidden');
                        }, 3000);
                    }else{
                        $('#message1').removeClass('hidden');
                        $('#message1').html("Maximum number is 4");
                        $('#message1').css("color",'#ff5652');
                        document.getElementById("avertissement").play();
                        setTimeout(function() {
                            $('#message1').addClass('hidden');
                        }, 3000);
                    }

                },
                error: function (e) {
                    console.log("erreur de creation player");

                    // Message for user
                    $('#message1').removeClass('hidden');
                    $('#message1').html("Participant not saved, try again");
                    $('#message1').css("color",'#ff5652');
                    setTimeout(function() {
                        $('#message1').addClass('hidden');
                    }, 3000);
                }
            });
        }
        disableButton("#user1-ready");
    });

    // mark user1 is ready
    $('#user1-ready').on('click',function (e){
        e.preventDefault();
        // change page
        $('#user1-second').addClass('hidden');
        $('#user1-six').removeClass('hidden');
        // send message to start game
        messageInput.value = "Team info";
        sendMessage();
    });

    // controler l'insertion dans age
    document.getElementById('age').addEventListener('input', function(event) {
        let inputValue = event.target.value;
        event.target.value = inputValue.replace(/\D/g, '');
    });

    // click gift user 1
    $('#click').on('click',function(e){
        e.preventDefault();
        $('.click-user1-1').addClass('transform-click');
        $('.sparkles-user1-1').addClass('transform-sparkles');
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                    if(item.user.loginName === "user1"){
                        team1Name = '@'+item.teamName;
                        team1Score = item.score;
                    }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });
                if(team1Score === team2Score){
                    $('.wishes-user1-1').html("Equality!");
                    $('.text-result-user1-1').html("Equality!");
                    $('#user1-message-final').html("Congratulations to both teams for having the same score, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                }else{
                    if(team1Score > team2Score){
                        $('.wishes-user1-1').html("You won!");
                        $('.text-result-user1-1').html("You won!");
                        $('#user1-message-final').html("Congratulation !! you are the winner, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                    }else{
                        $('.wishes-user1-1').html("You lost!");
                        $('.text-result-user1-1').html("You lost!");
                        $('#user1-message-final').html("You need an effort, Final score : Team2 = "+team2Score+" and Team1 = "+ team1Score);
                    }
                }

            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });
    $('#click3').on('click',function(e){
        e.preventDefault();
        $('.click-user1-2').addClass('transform-click');
        $('.sparkles-user1-2').addClass('transform-sparkles');
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                    if(item.user.loginName === "user1"){
                        team1Name = '@'+item.teamName;
                        team1Score = item.score;
                    }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });
                if(team1Score === team2Score){
                    $('.wishes-user1-2').html("Equality!");
                    $('.text-result-user1-2').html("Equality!");
                    $('#user3-message-final').html("Congratulations to both teams for having the same score, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                }else{
                    if(team1Score > team2Score){
                        $('.wishes-user1-2').html("You won!");
                        $('.text-result-user1-2').html("You won!");
                        $('#user3-message-final').html("Congratulation !! you are the winner, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                    }else{
                        $('.wishes-user1-2').html("You lost!");
                        $('.text-result-user1-2').html("You lost!");
                        $('#user3-message-final').html("You need an effort, Final score : Team2 = "+team2Score+" and Team1 = "+ team1Score);
                    }
                }

            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });

    // click gift user 2
    $('#click2').on('click',function(e){
        e.preventDefault();
        $('.click-user2-1').addClass('transform-click');
        $('.sparkles-user2-1').addClass('transform-sparkles');
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                    if(item.user.loginName === "user1"){
                        team1Name = '@'+item.teamName;
                        team1Score = item.score;
                    }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });
                if(team1Score === team2Score){
                    $('.wishes-user2-1').html("Equality!");
                    $('.text-result-user2-1').html("Equality!");
                    $('#user2-message-final').html("Congratulations to both teams for having the same score, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                }else{
                    if(team1Score > team2Score){
                        $('.wishes-user2-1').html("You lost!");
                        $('.text-result-user2-1').html("You lost!");
                        $('#user2-message-final').html("You need an effort, Final score : Team2 = "+team2Score+" and Team1 = "+ team1Score);
                    }else{
                        $('.wishes-user2-1').html("You won!");
                        $('.text-result-user2-1').html("You won!");
                        $('#user2-message-final').html("Congratulation !! you are the winner, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                    }
                }

            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });
    $('#click4').on('click',function(e){
        e.preventDefault();
        $('.click-user2-2').addClass('transform-click');
        $('.sparkles-user2-2').addClass('transform-sparkles');
        ajaxGetHtmlContents("/score-final", {})
            .done(function (donnee) {
                let team1Name = "@Null";
                let team2Name = "@Null";
                let team1Score = 0;
                let team2Score = 0;
                donnee.forEach(function (item){
                    if(item.user.loginName === "user1"){
                        team1Name = '@'+item.teamName;
                        team1Score = item.score;
                    }
                    if(item.user.loginName === "user2"){
                        team2Name = '@'+item.teamName;
                        team2Score = item.score;
                    }
                });
                if(team1Score === team2Score){
                    $('.wishes-user2-2').html("Equality!");
                    $('.text-result-user2-2').html("Equality!");
                    $('#user4-message-final').html("Congratulations to both teams for having the same score, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                }else{
                    if(team1Score > team2Score){
                        $('.wishes-user2-2').html("You lost!");
                        $('.text-result-user2-2').html("You lost!");
                        $('#user4-message-final').html("You need an effort, Final score : Team2 = "+team2Score+" and Team1 = "+ team1Score);
                    }else{
                        $('.wishes-user2-2').html("You won!");
                        $('.text-result-user2-2').html("You won!");
                        $('#user4-message-final').html("Congratulation !! you are the winner, Final score : Team2 = "+team2Score+" and Team1 = "+team1Score);
                    }
                }

            }).fail(function (err) {
            console.log("erreur de chargement");
        });
    });

});