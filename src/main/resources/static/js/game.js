'use strict';

var messageInput = document.querySelector('#message');
var stompClient = null;
var username = null;
var timerInterval = 0;
var timerInterval2 = 0;
var timeInSeconds = 300; // 5 minutes
var timeInSeconds2 = 60; // 1 minutes
var statusgame = "encoure";

function connect() {
    username = document.querySelector('#connect-user').value.trim();

    if(username) {
        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
}

// add user
function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username , type: 'JOIN'})
    )

}

function onError(error) {
    console.log("Impossible to connect");
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    if(message.type === 'JOIN') {
        console.log(message.sender+" is ready");
    } else if (message.type === 'LEAVE') {
        console.log(message.sender+" is leave");
    } else {
        console.log(message.sender+" is connected");
        // give access to game
        if(message.content === "Start game"){
            // play song for starting
            document.getElementById("letsgo").play();
            // change page
            $('#user1-six').addClass('hidden');
            $('#user1-thrid').removeClass('hidden');
            // Initial update
            updateCountdown();
            // Set up an interval to update the countdown every second
            timerInterval = setInterval(updateCountdown, 1000);
            // play song fnished
            setTimeout(function (){
                document.getElementById("sonortimerending").play();
            }, 270000);
        }
        if(message.content=== "End game1"){
            // for user
            //$('#user1-four').removeClass('hidden');
            $('#user1-resultat').removeClass('hidden');
            $('#user2-resultat').removeClass('hidden');
            if(!$('#user1-thrid').hasClass('hidden')){
                $('#user1-thrid').addClass('hidden');
                //$('#first-round').removeClass('hidden');
            }
            if(!$('#user1-five').hasClass('hidden')){
                $('#user1-five').addClass('hidden');
                //$('#first-round').removeClass('hidden');
                //$('#secound-round').removeClass('hidden');
            }
            // for admin
            if(!$('#admin-second').hasClass('hidden')){
                $('#admin-second').addClass('hidden');
            }
            if(!$('#admin-four').hasClass('hidden')){
                $('#admin-four').addClass('hidden');
            }
            $('#admin-third').removeClass('hidden');
            statusgame = "terminer";

            // applaudissement
            document.getElementById("terminate").play();
        }
        if(message.content=== "End game2"){
            // for user
            //$('#user1-four').removeClass('hidden');
            $('#user1-resultat-2').removeClass('hidden');
            $('#user2-resultat-2').removeClass('hidden');
            if(!$('#user1-thrid').hasClass('hidden')){
                $('#user1-thrid').addClass('hidden');
                //$('#first-round').removeClass('hidden');
            }
            if(!$('#user1-five').hasClass('hidden')){
                $('#user1-five').addClass('hidden');
                //$('#first-round').removeClass('hidden');
                //$('#secound-round').removeClass('hidden');
            }
            // for admin
            if(!$('#admin-second').hasClass('hidden')){
                $('#admin-second').addClass('hidden');
            }
            if(!$('#admin-four').hasClass('hidden')){
                $('#admin-four').addClass('hidden');
            }
            $('#admin-third').removeClass('hidden');
            statusgame = "terminer";

            // applaudissement
            document.getElementById("terminate").play();
        }
        if(message.content === "Team info"){
            // get team info
            ajaxGetHtmlContents("/get-teams-players/"+message.sender+"",{})
                .done(function (donnee) {
                    $('.team-list').append(donnee);
                }).fail(function (err) {
                console.log("erreur de chargement");
            });
            // activate button start after 2 team ready
            disableButton('#admin-start-game');

        }
        if((message.content).split('-')[0] === "score"){
           $("#"+message.sender+"-"+(message.content).split('-')[1]+"-response").html(""+(message.content).split('-')[2]+"");
        }
        if(message.content === "Second Round"){
            // play song for starting
            document.getElementById("letsgo").play();
            // go to the game question
            //$('#user1-four').addClass('hidden');
            $('#user1-resultat').addClass('hidden');
            $('#user2-resultat').addClass('hidden');
            $('#user1-five').removeClass('hidden');
            // go to statistique question
            $('#admin-third').addClass('hidden');
            $('#admin-four').removeClass('hidden');
            // lancer timer
            updateCountdown2();
            // Set up an interval to update the countdown every second
            timerInterval2 = setInterval(updateCountdown2, 1000);
            // play song fnished
            setTimeout(function (){
                document.getElementById("sonortimerending2").play();
            }, 54000);
        }
        if(message.content === "finished game"){
            // kill the socket
            stompClient.send("/app/chat.logoutUser",
                {},
                JSON.stringify({sender: "admin" , type: 'LEAVE'})
            )
            stompClient.send("/app/chat.logoutUser",
                {},
                JSON.stringify({sender: "user1" , type: 'LEAVE'})
            )
            stompClient.send("/app/chat.logoutUser",
                {},
                JSON.stringify({sender: "user2" , type: 'LEAVE'})
            )
            window.location.reload();
        }
    }
}

// send message
function sendMessage() {
    if(stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
}

// timer first round
const countdownElement = document.getElementById('countdown');
const countdownadminElement = document.getElementById('countdownadmin');
function updateCountdown() {
    const minutes = Math.floor(timeInSeconds / 60);
    const seconds = timeInSeconds % 60;
    // Format the time with leading zeros
    const formattedTime = `00:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
    // Update the HTML content
    if(countdownElement !== null){
        countdownElement.textContent = formattedTime;
    }
    if(countdownadminElement !== null){
        countdownadminElement.textContent = formattedTime;
    }
    // Check if the countdown has reached zero
    if (timeInSeconds === 0) {
        clearInterval(timerInterval);
    } else {
        // Decrement the time by 1 second
        timeInSeconds--;
    }
}

// timer second round
const countdownElement2 = document.getElementById('countdown2');
const countdownadminElement2 = document.getElementById('countdownadmin2');
function updateCountdown2() {
    const minutes = Math.floor(timeInSeconds2 / 60);
    const seconds = timeInSeconds2 % 60;
    // Format the time with leading zeros
    const formattedTime = `00:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
    // Update the HTML content
    if(countdownElement2 !== null){
        countdownElement2.textContent = formattedTime;
    }
    if(countdownadminElement2 !== null){
        countdownadminElement2.textContent = formattedTime;
    }
    // Check if the countdown has reached zero
    if (timeInSeconds2 === 0) {
        clearInterval(timerInterval2);
    } else {
        // Decrement the time by 1 second
        timeInSeconds2--;
    }
}

window.addEventListener('beforeunload', function (e) {
    if(statusgame === "encoure"){
        e.preventDefault();
        return false;
    }else{
        return true;
    }
});


