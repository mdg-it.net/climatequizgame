package com.unfpa.climatequizgame.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="questions")
@ToString(exclude = {"responseList"})
public class Question extends AbstractModel{

    private String question;

    private CategoryGame category;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,mappedBy = "question")
    private List<Response> responseList;
}
