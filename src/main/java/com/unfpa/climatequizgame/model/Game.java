package com.unfpa.climatequizgame.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="game")
public class Game extends AbstractModel{

    @ManyToOne(cascade = CascadeType.ALL)
    private Team team1;

    @ManyToOne(cascade = CascadeType.ALL)
    private Team team2;

}
