package com.unfpa.climatequizgame.model;

import lombok.Data;

@Data
public class FormData {
    private String questionId;
    private String responseId;
}
