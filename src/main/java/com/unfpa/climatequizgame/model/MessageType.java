package com.unfpa.climatequizgame.model;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE
}
