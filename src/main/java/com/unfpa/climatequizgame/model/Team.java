package com.unfpa.climatequizgame.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="teams")
@ToString(exclude = {"playersList"})
public class Team extends AbstractModel{

    private String teamName;

    private int score;

    @ManyToOne
    @JoinColumn(name = "users",referencedColumnName = "id")
    private User user;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,mappedBy = "team")
    List<Players> playersList;

}
