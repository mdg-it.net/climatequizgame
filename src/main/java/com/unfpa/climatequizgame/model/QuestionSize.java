package com.unfpa.climatequizgame.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionSize {

    private int numero;

    private int questionId;
}
