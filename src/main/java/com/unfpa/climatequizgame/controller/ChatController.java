package com.unfpa.climatequizgame.controller;

import com.unfpa.climatequizgame.model.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import java.util.Objects;

@Controller
public class ChatController {

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage){
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor){
        // Add username in websocket session
        headerAccessor.getSessionAttributes().put("username",chatMessage.getSender());
        return chatMessage;
    }

    @MessageMapping("/chat.logoutUser")
    @SendTo("/topic/public")
    public ChatMessage logoutUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor){
        // logout all username in websocket session
        headerAccessor.getSessionAttributes().remove("username",chatMessage.getSender());
        return chatMessage;
    }

}
