package com.unfpa.climatequizgame.controller;

import com.unfpa.climatequizgame.model.*;
import com.unfpa.climatequizgame.service.Implementation.PlayerService;
import com.unfpa.climatequizgame.service.Implementation.TeamResponseService;
import com.unfpa.climatequizgame.service.Implementation.TeamService;
import com.unfpa.climatequizgame.service.QuestionService;
import com.unfpa.climatequizgame.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user1")
public class GameRestApi {

    private final QuestionService questionService;
    private final ResponseService responseService;
    private final TeamService teamService;
    private final PlayerService playerService;
    private final TeamResponseService teamResponseService;

    public GameRestApi(QuestionService questionService, ResponseService responseService, TeamService teamService, PlayerService playerService, TeamResponseService teamResponseService) {
        this.questionService = questionService;
        this.responseService = responseService;
        this.teamService = teamService;
        this.playerService = playerService;
        this.teamResponseService = teamResponseService;
    }

    @PostMapping("/save-data-user1")
    public ResponseEntity<String> responseQuestion1(@ModelAttribute FormData formdata, @RequestParam("question") String question){
        String value = "0-0";
        // get information of connexion authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User us = (User)authentication.getPrincipal();
        // get team
        Team team = teamService.getTeamByUserAndStatus(us,false);
        // check response if true
        if(formdata.getResponseId() != null && question != null && team != null) {
            Question question1 = questionService.getQuestionById(Long.parseLong(question));
            Response responseCorrect = responseService.getTrueResponseByQuestion(question1, true);
            Response response = responseService.getResponseById(Long.parseLong(formdata.getResponseId()));
            // save team response
            TeamResponse teamResponse = new TeamResponse();
            teamResponse.setResponse(response);
            teamResponse.setTeam(team);
            this.teamResponseService.save(teamResponse);
            // update or not team score
            if (responseCorrect.getId() == Long.parseLong(formdata.getResponseId())) {
                value = ""+question+"-1";
                team.setScore(team.getScore() + 1);
                teamService.saveTeam(team);
            }else{
                value = ""+question+"-0";
            }
        }
        return new ResponseEntity<>(value, HttpStatus.OK);
    }

    @PostMapping("/save-participant")
    public ResponseEntity<Players> savePlayer(@ModelAttribute Players player,@ModelAttribute("age") String age){
        // get information of connexion authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User us = (User)authentication.getPrincipal();
        // check if player is less than 4
        Team team = teamService.getTeamByUserAndStatus(us,false);
        player.setTeam(team);
        player.setAge(Integer.parseInt(age));
        List<Players> playersList = playerService.getListPlayersByTeam(team);
        Players player1 = new Players();
        if(playersList.size() < 4){
            player1 = playerService.savePlayers(player);
        }
        return new ResponseEntity<>(player1, HttpStatus.OK);
    }

    @PostMapping("/save-team")
    public ResponseEntity<Team> saveTeam(@ModelAttribute Team team){
        // get information of connexion authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User us = (User)authentication.getPrincipal();
        //check if the session is used
        Team teamInBase = teamService.getTeamByUserAndStatus(us,false);
        Team team1 = new Team();
        if(teamInBase == null) {
            // save team
            team.setScore(0);
            team.setUser(us);
            team.setStatus(false);
            team1 = teamService.saveTeam(team);
        }
        return new ResponseEntity<>(team1, HttpStatus.OK);
    }

}
