package com.unfpa.climatequizgame.controller;

import com.unfpa.climatequizgame.model.*;
import com.unfpa.climatequizgame.service.GameService;
import com.unfpa.climatequizgame.service.Implementation.PlayerService;
import com.unfpa.climatequizgame.service.Implementation.TeamService;
import com.unfpa.climatequizgame.service.QuestionService;
import com.unfpa.climatequizgame.service.ResponseService;
import com.unfpa.climatequizgame.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GameController {

    private final QuestionService questionService;
    private final ResponseService responseService;
    private final TeamService teamService;
    private final GameService gameService;
    private final UserService userService;
    private final PlayerService playerService;


    public GameController(QuestionService questionService, ResponseService responseService, TeamService teamService, GameService gameService, UserService userService, PlayerService playerService) {
        this.questionService = questionService;
        this.responseService = responseService;
        this.teamService = teamService;
        this.gameService = gameService;
        this.userService = userService;
        this.playerService = playerService;
    }

    @GetMapping("/go-to-play")
    public String goToPlay(Model model){

        // get information of connexion authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User us = (User)authentication.getPrincipal();
        model.addAttribute("userDetails",authentication.getPrincipal());

        List<Question> questions1 = this.questionService.getAllQuestion().stream()
                .filter(f->f.getCategory().equals(CategoryGame.FIRST_ROUND)).collect(Collectors.toList());
        List<Question> questions2 = this.questionService.getAllQuestion().stream()
                .filter(f->f.getCategory().equals(CategoryGame.SECOND_ROUND)).collect(Collectors.toList());
        model.addAttribute("questions",questions1);
        model.addAttribute("questions2",questions2);
        model.addAttribute("responses",this.responseService.getAllResponse());

        int sizeQuestion = questions1.size();
        List<QuestionSize> questionsSize = new ArrayList<>();
        int i=0;
        for(Question qs : questions1){
            QuestionSize qst = new QuestionSize();
            qst.setNumero(i);
            qst.setQuestionId((int) qs.getId());
            questionsSize.add(qst);
            i++;
        }
        model.addAttribute("questionsSize",questionsSize);
        model.addAttribute("numberQuestion",sizeQuestion);

        int sizeQuestion2 = questions2.size();
        List<QuestionSize> questionsSize2 = new ArrayList<>();
        int j=0;
        for(Question qs : questions2){
            QuestionSize qst = new QuestionSize();
            qst.setNumero(j);
            qst.setQuestionId((int) qs.getId());
            questionsSize2.add(qst);
            j++;
        }
        model.addAttribute("questionsSize2",questionsSize2);
        model.addAttribute("numberQuestion2",sizeQuestion2);

        model.addAttribute("formdata",new FormData());

        model.addAttribute("player",new Players());

        model.addAttribute("team",new Team());

        return "game";
    }

    @GetMapping("/create-game")
    public String createGame(Model model){
        // create game
        User us1 = userService.getUserByLoginName("user1").get();
        User us2 = userService.getUserByLoginName("user2").get();
        Team t1 = teamService.getTeamByUserAndStatus(us1,false);
        Team t2 = teamService.getTeamByUserAndStatus(us2,false);
        Game game = new Game();
        game.setTeam1(t1);
        game.setTeam2(t2);
        gameService.createGame(game);
        return "score::create-game";
    }

    @GetMapping("/score-final")
    @ResponseBody
    public List<Team> getScoreFinal(Model model){
        return teamService.getTeamByStatus(false);
    }

    @GetMapping("/get-teams-players/{user}")
    public String getTeamsPlayers(Model model, @PathVariable ("user") String user){
        User user1 = userService.getUserByLoginName(user).get();
        Team team = teamService.getTeamByUserAndStatus(user1,false);
        List<Players> playersList = new ArrayList<>();
        if(team != null) {
            playersList.addAll(playerService.getListPlayersByTeam(team));
        }
        model.addAttribute("team",team);
        model.addAttribute("players",playersList);
        return "score::get-teams-players";
    }

    @GetMapping("/update-data")
    public String updateData(Model model){
        // update team
         List<Team> teams = teamService.getTeamByStatus(false);
         for(Team tm : teams){
             tm.setStatus(true);
             teamService.saveTeam(tm);
         }
         // update game
        List<Game> gameList = gameService.getGameByStatus(false);
         for(Game gm: gameList) {
             gm.setStatus(true);
             gameService.createGame(gm);
         }

        return "score::game-finished";
    }

    @GetMapping("/update-data/user1")
    @ResponseBody
    public String updateDataUser1(Model model){
        // update team1
        User user1 = userService.getUserByLoginName("user1").get();
        Team team = teamService.getTeamByUserAndStatus(user1,false);
        if(team != null) {
            team.setStatus(true);
            teamService.saveTeam(team);
        }
        return "Team1 update";
    }

    @GetMapping("/update-data/user2")
    @ResponseBody
    public String updateDataUser2(Model model){
        // update team2
        User user2 = userService.getUserByLoginName("user2").get();
        Team team = teamService.getTeamByUserAndStatus(user2,false);
        if(team != null) {
            team.setStatus(true);
            teamService.saveTeam(team);
        }
        return "Team2 update";
    }

}
