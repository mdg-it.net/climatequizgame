package com.unfpa.climatequizgame.service;

import com.unfpa.climatequizgame.model.Question;

import java.util.List;

public interface QuestionService {

    Question saveOrUpdateQuestion(Question question);

    List<Question> getAllQuestion();

    Question getQuestionById(Long questionId);

}
