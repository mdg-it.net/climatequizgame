package com.unfpa.climatequizgame.service;

import com.unfpa.climatequizgame.model.Question;
import com.unfpa.climatequizgame.model.Response;

import java.util.List;

public interface ResponseService {

    Response saveOrUpdateQuestion(Response response);

    List<Response> getAllResponse();

    Response getTrueResponseByQuestion(Question question, boolean rightOrFalse);

    Response getResponseById(Long id);
}
