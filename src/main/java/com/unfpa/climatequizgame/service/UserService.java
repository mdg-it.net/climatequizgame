package com.unfpa.climatequizgame.service;

import com.unfpa.climatequizgame.model.User;

import java.util.List;
import java.util.Optional;


public interface UserService {

    void saveUser(User user);

    List<User> getUserInBase();

    Optional<User> getUserByLoginName(String admin);

}
