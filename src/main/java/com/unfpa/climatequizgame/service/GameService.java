package com.unfpa.climatequizgame.service;

import com.unfpa.climatequizgame.model.Game;
import com.unfpa.climatequizgame.model.Question;

import java.util.List;

public interface GameService {

    Game createGame(Game game);

    List<Game> getGameByStatus(Boolean b);

}
