package com.unfpa.climatequizgame.service.Implementation;

import com.unfpa.climatequizgame.model.Question;
import com.unfpa.climatequizgame.model.Response;
import com.unfpa.climatequizgame.repository.ResponseRepository;
import com.unfpa.climatequizgame.service.ResponseService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseServiceImpl implements ResponseService {
    private final ResponseRepository responseRepository;

    public ResponseServiceImpl(ResponseRepository responseRepository) {
        this.responseRepository = responseRepository;
    }

    @Override
    public Response saveOrUpdateQuestion(Response response) {
        return this.responseRepository.save(response);
    }

    @Override
    public List<Response> getAllResponse() {
        return this.responseRepository.findAll();
    }

    @Override
    public Response getTrueResponseByQuestion(Question question, boolean rightOrFalse) {
        return this.responseRepository.findByQuestionAndIsCorrect(question,rightOrFalse);
    }

    @Override
    public Response getResponseById(Long id) {
        return this.responseRepository.findById(id).get();
    }
}
