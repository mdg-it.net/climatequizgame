package com.unfpa.climatequizgame.service.Implementation;

import com.unfpa.climatequizgame.model.Game;
import com.unfpa.climatequizgame.repository.GameRepository;
import com.unfpa.climatequizgame.service.GameService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImpl implements GameService {
    private final GameRepository gameRepository;

    public GameServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public Game createGame(Game game) {
        return this.gameRepository.save(game);
    }

    @Override
    public List<Game> getGameByStatus(Boolean b) {
        return this.gameRepository.findByStatus(b);
    }
}
