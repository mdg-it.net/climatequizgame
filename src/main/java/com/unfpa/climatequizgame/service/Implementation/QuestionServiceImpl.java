package com.unfpa.climatequizgame.service.Implementation;

import com.unfpa.climatequizgame.model.Question;
import com.unfpa.climatequizgame.repository.QuestionRepository;
import com.unfpa.climatequizgame.service.QuestionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question saveOrUpdateQuestion(Question question) {
        return this.questionRepository.save(question);
    }

    @Override
    public List<Question> getAllQuestion() {
        return this.questionRepository.findAll();
    }

    @Override
    public Question getQuestionById(Long questionId) {
        return this.questionRepository.findById(questionId).get();
    }

}
