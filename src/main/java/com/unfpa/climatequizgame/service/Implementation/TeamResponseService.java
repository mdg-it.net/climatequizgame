package com.unfpa.climatequizgame.service.Implementation;

import com.unfpa.climatequizgame.model.TeamResponse;
import com.unfpa.climatequizgame.repository.TeamResponseRepository;
import org.springframework.stereotype.Service;

@Service
public class TeamResponseService {

    private final TeamResponseRepository teamResponseRepository;

    public TeamResponseService(TeamResponseRepository teamResponseRepository) {
        this.teamResponseRepository = teamResponseRepository;
    }

    public TeamResponse save(TeamResponse teamResponse){
        return teamResponseRepository.save(teamResponse);
    }
}
