package com.unfpa.climatequizgame.repository;

import com.unfpa.climatequizgame.model.Question;
import com.unfpa.climatequizgame.model.Response;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponseRepository extends JpaRepository<Response, Long> {

    Response findByQuestionAndIsCorrect(Question question,Boolean rightOrFalse);
}
