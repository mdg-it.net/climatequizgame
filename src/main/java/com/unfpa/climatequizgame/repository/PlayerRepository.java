package com.unfpa.climatequizgame.repository;

import com.unfpa.climatequizgame.model.Players;
import com.unfpa.climatequizgame.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Players, Long> {

    List<Players> findByTeam(Team team);
}
