package com.unfpa.climatequizgame.repository;

import com.unfpa.climatequizgame.model.Team;
import com.unfpa.climatequizgame.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Team findByUserAndStatus(User user,Boolean status);

    List<Team> findByStatus(Boolean b);
}
