package com.unfpa.climatequizgame.repository;

import com.unfpa.climatequizgame.model.TeamResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamResponseRepository extends JpaRepository<TeamResponse,Long> {
}
